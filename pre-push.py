#!/usr/bin/python

git_binary_path = "/usr/bin/git"

import subprocess, sys, os

try:
    text = subprocess.check_output(
	    [git_binary_path, "diff", "--name-only", "HEAD@{1}"],
	    stderr=subprocess.STDOUT).decode("utf-8") #HEAD@{1} = prev commit
	                                              #HEAD@{0} = cur commit for testing
    file_list = text.splitlines()
    
    batch_files = list() #contains file path from the git directory on
    batch_file_end = "ImportConfig.xml"
    for file_s in file_list:
        if file_s.endswith(batch_file_end):
            batch_files.append(file_s)

    if len(batch_files):
        args = ["%s/gitPushAlert.py" % os.getcwd(),]
        filestodo = ["%s/%s" % (os.getcwd(),f) for f in batch_files]
        args.extend(filestodo)
        
        print "--- found changes in ImportConfig to report ---"
        print "---    calling %s ---" % " ".join(args)
        subprocess.call(" ".join(args), shell=True)

    else:
        print "--- no ImportConfig.xml change to report to DevOps ---"
    
except:
    print "something went wrong in pre-push.py"
    sys.exit(12)
