#! /usr/bin/python

import argparse
from email import encoders
from email.MIMEBase import MIMEBase
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
import ntpath
import os
import smtplib
import subprocess
import sys


git_binary_path = "/usr/bin/git"


def gitUserName():
    try:
        text = subprocess.check_output(
            [git_binary_path, "config", "user.name"], stderr=subprocess.STDOUT).decode("utf-8")
        return text.splitlines()[0]
    except:
        return ""

def gitUserEmail():
    try:
        text = subprocess.check_output(
            [git_binary_path, "config", "user.email"], stderr=subprocess.STDOUT).decode("utf-8")
        return text.splitlines()[0]
    except:
        return ""


def gitDifPerFile(filePath):
    dif = []
    if filePath:
        text = subprocess.check_output(
	        [git_binary_path, "diff", "HEAD@{1}", filePath],
	        stderr=subprocess.STDOUT).decode("utf-8") #HEAD@{1} = prev commit
        dif = text.splitlines()
    return dif
    

def formatTwoPageDiff(lines):
    #formats a list of diff into two pages diff 
    separator = " | "
    maxColLen = max([len(k) for k in lines[2:]]) + len(separator)
    ret = lines[:2]
    for line in lines[2:]:
        if line[0] == "+": #write the diff to the right
            ret.append((separator + line).rjust(maxColLen + len(line)))
        else:              #the rest to the left
            ret.append(line.ljust(maxColLen - len(separator)) + separator)
    return ret
    

def createFile(filePath, lines):
    try:
        dirPath = os.path.dirname(filePath)
        if not os.path.exists(dirPath):
            os.makedirs(dirPath)
        with open(filePath, "w") as f:
            for line in lines:
                f.write(line + os.linesep)
        return True
    except:
        print "--- error in createFile %s --- " % filePath
        print sys.exc_info()
        return False

def deleteFile(filePath):
    try:
        dirPath = os.path.dirname(filePath)
        if os.path.exists(dirPath) and os.path.exists(filePath):
            os.remove(filePath)
        return True
    except:
        print "--- error in deleteFile %s --- " % filePath
        print sys.exc_info()
        return False


def sendAlert(filePaths):
    for filePath in filePaths:
        fileName = ntpath.basename(filePath)
        fileNamNoExt = fileName.split('.')[0]
        lines = ["git diff HEAD@{1} %s\n\n" % filePath,]   #HEAD@{1} = prev commit, 
                                                           #HEAD@{0} = cur commit, for testing
        try:
            lines.extend(gitDifPerFile(filePath))
        except:
            print sys.exc_info()
            continue
        body = lines 
        attachFileName = "%s/temp/%s.diff" % (os.environ["HOME"], fileNamNoExt)
        if not createFile(attachFileName, formatTwoPageDiff(lines)):
            return
        
        #user credentials
        userName = gitUserName()
        userEmail = gitUserEmail()
        
        # Create a text/plain message
        msg = MIMEMultipart("mixed")
        msg['Subject'] = "%s changed %s" % (userName, fileName)
        msg['From'] = userEmail 
        msg['To'] = ", ".join([userEmail, "ian.hayfield@ancoa.com"]) 
        msg.attach(MIMEText("\n".join(body), "plain"))
        with open(attachFileName, "rb") as attachment:
            part = MIMEBase('application', 'octet-stream')
            part.set_payload((attachment).read())
            encoders.encode_base64(part)
            part.add_header('Content-Disposition', "attachment; filename= %s" % attachFileName)
            msg.attach(part)

        # Send the message via your own SMTP server
        smtp = smtplib.SMTP('localhost')
        smtp.ehlo()                     #print smtp.esmtp_features
        #using my own account
        #smtp = smtplib.SMTP('smtp.gmail.com', 587)
        #smtp.starttls()
        #smtp.login("user", "password")
        smtp.sendmail(msg['From'], msg['To'], msg.as_string())
        smtp.quit()
        
        deleteFile(attachFileName)



if __name__ == "__main__":
    try:
        parser = argparse.ArgumentParser(
                    description='alert dev-ops of git file changes')
        parser.add_argument('files', metavar='files', nargs='+',
                            help='one or more files', type=str)
        filePaths = parser.parse_args().files 
        fileNames = [ntpath.basename(k) for k in filePaths]
        
        print "--- gitPushAlerts.py reporting changes in files [%s] --- " % ", ".join(fileNames)
        sendAlert(filePaths)
            
    except:
        print "--- something wrong in gitPushAlert.py --- "
        print sys.exc_info()
        

